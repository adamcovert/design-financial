var ready = function ( fn ) {
  // Sanity check
  if ( typeof fn !== 'function' ) return;

  // If document is already loaded, run method
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }

  // Otherwise, wait until document is loaded
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

ready(function () {

  var promoPhotos = document.querySelectorAll('.promo__photo');
  promoPhotos.forEach(function (elem) {
    if (elem.classList.contains('is-hide')) {
      if (elem.classList.contains('promo__photo--rest')) {
        setTimeout(function () {
          elem.classList.remove('is-hide');
        }, 2200)
      }
      if (elem.classList.contains('promo__photo--college')) {
        setTimeout(function () {
          elem.classList.remove('is-hide');
        }, 2000)
      }
      if (elem.classList.contains('promo__photo--coffee')) {
        setTimeout(function () {
          elem.classList.remove('is-hide');
        }, 2100)
      }
      if (elem.classList.contains('promo__photo--retired')) {
        setTimeout(function () {
          elem.classList.remove('is-hide');
        }, 2300)
      }
    }
  });

  var promoShapes = document.querySelectorAll('.promo__shape');
  promoShapes.forEach(function (shape) {
    if (shape.classList.contains('promo__shape--main')) {
      setTimeout(function () {
        shape.classList.remove('is-hide');
      }, 2400)
    }
    if (shape.classList.contains('promo__shape--secondary')) {
      setTimeout(function () {
        shape.classList.remove('is-hide');
      }, 2500)
    }
  });

  var promoSlogan = document.querySelector('.promo__slogan');
  setTimeout(function () {
    promoSlogan.classList.remove('is-hide');
  }, 2500)

  svg4everybody();

  var viewportWidth = window.innerWidth || document.documentElement.clientWidth;
  if (viewportWidth > 992) {
    modals.closeModal();
    modals.destroy();
    // tabby.init();
  } else {
    modals.init();
    // tabby.destroy();
  }

  window.addEventListener('resize', function () {
    var viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if (viewportWidth > 992) {
      modals.closeModal();
      modals.destroy();
      // tabby.init();
    } else {
      modals.init();
      // tabby.destroy();
    }
  }, false);

  /**
   * NodeList.prototype.forEach() polyfill
   * https://developer.mozilla.org/en-US/docs/Web/API/NodeList/forEach#Polyfill
   */
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }

  // Find all tab links
  var tabLinks = document.querySelectorAll('.financial-journey__link');

  // Loop through
  tabLinks.forEach(function (tab) {

    // If hover
    tab.addEventListener('mouseenter', function () {

      var content = document.querySelector(event.target.hash);
      if (!content) return;

      event.preventDefault();

      tabLinks.forEach(function (tabLink) {
        tabLink.classList.remove('active');
      });

      // Remove active class from all tabs content
      var tabItems = document.querySelectorAll('.financial-journey__tab');
      tabItems.forEach(function (tabItem) {
        tabItem.classList.remove('active');
      });

      // Add active class to target
      tab.classList.add('active');
      content.classList.add('active');
    }, false);
  });

});